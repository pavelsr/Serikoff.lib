# Usage

Below are different options how to connect my library to your project.
You can use any.


## simple with abs path

```
use lib "/home/pavel/perl_libs";  # path serikoff.lib
use Serikoff::Yandex qw(geocoder);
```

## simple with relative path

```
use './serikoff.lib';
use Serikoff::Kayako qw(api_get_ticket_xml api_get_ticket_hash);
```

(you have serikoff.lib folder in a root folder of your projet)


## via setting enviroment variable

```
use lib $ENV {DEVLOCATION} ;
use Serikoff::Skud qw(monitor_file);
```

DEVLOCATION must be abs path


```
use lib $ENV {PWD}; ;
use Serikoff::Skud qw(monitor_file);
```

Search "Serikoff" folder in a folder from what your script is executed
If you want to execute your script from different locations (e.g. via cron) don't use it.


## via [FindBin](https://metacpan.org/pod/FindBin)


```
use lib $FindBin::Bin;
use Serikoff::Kayako qw(api_get_ticket_xml api_get_ticket_hash);
```

(in that case "Serikoff" (not a serikoff.lib) folder must be in a root of your projet. that can conflict README.md of your project)


```
use lib "$FindBin::Bin/serikoff.lib";
use Serikoff::Skud qw(monitor_file);
```

(more safer)


## via [Cwd](https://metacpan.org/pod/Cwd)


```
use Cwd;
my $dir = getcwd;   # name of directory from where script is calling
use lib "$dir/serikoff.lib";
use Serikoff::Skud qw(monitor_file);
```

I didn't recommend to use Cwd if you want to execute your scripts from different locations  (e.g. via cron). Same as ```use lib $ENV {PWD}```
Also I think that Cwd is king of useless cause e.g ```abs_path($0)``` and ```$0``` returns same.



## using [File::Basename](https://metacpan.org/pod/File::Basename);

```
use File::Basename qw( dirname );
use lib dirname($0).'/serikoff.lib';
```

Personally I think options with FindBin and File::Basename are best


### you can combine usage also

```
use lib $ENV {DEVLOCATION} || '/home/pavel/projects/serikoff.lib/';
use Serikoff::Skud qw(monitor_file);
```

```
use Cwd 'abs_path';
use File::Basename qw(dirname);
use lib dirname(abs_path($0)).'/serikoff.lib';
```

(thought in this case I think CWd is useless)


# How to set enviromental variables in Linux?


you can set it temporarily and check via terminal:

```
export DEVLOCATION="/home/pavel/projects/serikoff.lib/"
echo $DEVLOCATION
```

or persistent (to /etc/environment)

```
sudo -s
echo 'DEVLOCATION="/home/pavel/projects/serikoff.lib/"' >> /etc/environment
```


to check all env. variables run 

```printenv```