package Serikoff::Kayako;

use common::sense;
use Mojo::UserAgent;
use Mojo::Util;
use Digest::SHA qw(hmac_sha256_base64);
use XML::XML2JSON;
use utf8;
use Data::Dumper;


# use Google::Data::JSON;

use Exporter qw(import);
our @EXPORT_OK = qw(api_get_ticket_xml api_get_ticket_hash api_get);

my $XML2JSON = XML::XML2JSON->new(content_key => 'text', pretty => 1, attribute_prefix => 'attr_');
my $ua = Mojo::UserAgent->new;

# Get info about ticket in native XML

sub api_get_ticket_xml {
  my ($ticket_id, $auth_hash) = @_;
  my @alphabet = (('a'..'z'), ('A'..'Z'), 0..9);
  my $len = 10;
  my $salt = join '', map {$alphabet[rand(@alphabet)]} 1..$len;
  my $digest = hmac_sha256_base64($salt, $auth_hash->{secret_key}).'=';
  my %hash = (
        "apikey" => $auth_hash->{api_key},
        "salt" => $salt,
        "signature" => $digest,
        "e" => "/Tickets/Ticket/".$ticket_id."/"
   );
  my $info = $ua->get($auth_hash->{api_url} => form => \%hash)->res; #Mojo::Message::Response
  
  # my $xml = Mojo::Util::b64_encode $info->body, "\n";
  my $xml = $info->body; # Mojo::Asset::Memory. same methods as Mojo::Asset
  
  # commented expressions are not working
  #$xml =~ s/[^[:ascii:]]//g;  
  #$xml =~ s/[\d128-\d255]//g;
  #$xml =~ s/[^[:print:]]//;
  #$xml =~  s/[^[:print:]]+//g;
  $xml =~  s/([\x00-\x09]+)|([\x0B-\x1F]+)//g;  # fix non-printable characters problem. this pattern won't remove newline char
  return $xml;
};

sub api_get_ticket_hash {
  my ($ticket_id, $auth_hash) = @_;
  my $info = api_get_ticket_xml($ticket_id, $auth_hash);
  my $hash = {};
  if ($info eq 'Ticket not Found' || $info eq '') {
    $hash->{'attr_id'} = $ticket_id;
    $hash->{'is_found'} = 0; 
  } else { # is xml looks like xml
    # solve problem of non-printable character
    #$hash = b64_encode($hash);
    # warn $XML2JSON->xml2obj($info);
    $hash = $XML2JSON->xml2obj($info)->{'tickets'}->{'ticket'};
  }
  return $hash;
};


sub api_get {  # abstract api GET query
  my ($params, $auth_hash) = @_;
  my @alphabet = (('a'..'z'), ('A'..'Z'), 0..9);
  my $len = 10;
  my $salt = join '', map {$alphabet[rand(@alphabet)]} 1..$len;
  my $digest = hmac_sha256_base64($salt, $auth_hash->{secret_key}).'=';
  my %hash = (
    "apikey" => $auth_hash->{api_key},
    "salt" => $salt,
    "signature" => $digest,
  );
  %hash = (%hash, %$params);
  my $xml = $ua->get($auth_hash->{api_url} => form => \%hash)->res->body;
  $xml =~  s/([\x00-\x09]+)|([\x0B-\x1F]+)//g;
  my $hash = $XML2JSON->xml2obj($xml);
  return $hash;
}