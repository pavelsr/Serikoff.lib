package Serikoff::Datetime;

use common::sense;
use DateTime;
use Switch;

use Exporter qw(import);
our @EXPORT_OK = qw(yesterday start_day_of_current_week get_dt_periods);

sub yesterday {
  # return correct DateTime object for correct yesterday handling in case if yesterday was in previous month or year
  my $year = DateTime->now()->year();
  my $month = DateTime->now()->month();
  my $day_today = DateTime->now()->day();
  my $day = $day_today - 1;
  if ($day_today == 1 && $month == 1) {   # 1 of January
    $year = $year - 1;
    $month = 12;
    $day = 31;
  }
  if ($day_today == 1 && $month != 1) {   # 1st day of month
    $month = $month - 1;
    $day = DateTime->last_day_of_month( year => $year, month => $month );
  }
  return DateTime->new(year => $year, month => $month, day => $day);   # 00:00:00 of yesterday
};

sub start_day_of_current_week {
  # Solve other month on this week issue and eturn correct DateTime object
  my $res;
  my $now = DateTime->now();
  my $diff = $now->day() - $now->day_of_week() + 1;
  if ($diff <= 0) {
    $res = DateTime->new( 
      year => $now->year(), 
      month=> $now->month() - 1, 
      day => DateTime->last_day_of_month(year=>$now->year(), month => $now->month() -1)->day() + $diff
      );
  } else {
    $res = DateTime->new(
    year => $now->year(), 
    month => $now->month(), 
    day => $diff
    )
   }
   return $res;   # 00:00:00 of Monday of current week
};

sub get_dt_periods {
  # return period in epoch
  my $period = shift;
  my $h = {};
  my $now = DateTime->now();
  switch($period) {
   case 'today'  {
    $h->{dt_start} = DateTime->today()->epoch();
    $h->{dt_end} = $now->epoch();
    }
   case 'yesterday'  {  
    $h->{dt_start} = yesterday()->epoch();
    $h->{dt_end} = DateTime->today()->epoch();
   }
   case 'week'  {  
    $h->{dt_start} = start_day_of_current_week()->epoch();
    $h->{dt_end} = $now->epoch();
   }
   case 'month'  {  
    $h->{dt_start} = DateTime->new( year => $now->year(), month => $now->month() )->epoch();
    $h->{dt_end} = $now->epoch();
   }
   case 'past_month'  {  
    $h->{dt_start} = DateTime->new(year => $now->year(), month => $now->month()-1)->epoch();
    $h->{dt_end} = DateTime->new(year => $now->year(), month => $now->month())->epoch();
   }
  }
  return $h;
};