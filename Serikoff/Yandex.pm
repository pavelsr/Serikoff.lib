package Serikoff::Yandex;

use strict;
use warnings;
use Mojo::UserAgent;
use utf8;

use Exporter qw(import);
our @EXPORT_OK = qw(geocoder);

# direct geocoding using Yandex Maps API
sub geocoder {
	my $address = shift;
	my $base_url="http://geocode-maps.yandex.ru/1.x/?format=json&geocode=";
	my $ua = Mojo::UserAgent->new;
	# my $coordinates = $ua->get($base_url . encode('UTF-8', $address))->res->json->{response}->{GeoObjectCollection}->{featureMember}->[0]->{GeoObject}->{Point}->{pos};
	my $coordinates = $ua->get($base_url . $address)->res->json->{response}->{GeoObjectCollection}->{featureMember}->[0]->{GeoObject}->{Point}->{pos};
	return $coordinates;	# longitude, latitude
}