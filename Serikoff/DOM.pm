package Serikoff::DOM;

use warnings;
use Mojo::UserAgent;
use List::MoreUtils qw( each_array );
use Exporter qw(import);
use Encode;
use feature 'say';
use Data::Dumper;

our @EXPORT_OK = qw(table_to_hash table2array_of_hashes);


sub table_to_hash {
	my ($url, $container) = @_; 
	# say "url received: ".$url; 
	my $h = {};
	my $ua = Mojo::UserAgent->new;
	my $table_dom = $ua->get($url)->res->dom->at($container); 	# return UTF-8 Mojo::DOM object
	for my $i ($table_dom->find("tr")->each) {
		my $key_candidate = $i->find("td")->[0]->all_text;
		my $val_candidate = $i->find("td")->[1]->all_text;
		$key_candidate =~ s/[\$#@~!&;:]+//g;					# delete special symbols
		$key_candidate = decode('UTF-8', $key_candidate);
		$h->{$key_candidate} =  decode('UTF-8', $val_candidate);
		# says must be empty
		# say "encoding of key :" . Encode::Detect::Detector::detect($key_candidate);
		# say "encoding of value :" . Encode::Detect::Detector::detect($val_candidate); 
	}
	return $h;
}

sub table2array_of_hashes {
	my ($url, $container, $fields_arr) = @_;
	my @array_of_hashes;	#result
	my $ua = Mojo::UserAgent->new;
	my $dom = $ua->get($url)->res->dom->at($container);
	my $fields;		# array of fields
	if (defined $fields_arr) {
		$fields = $fields_arr;
	} else {
		for ($dom->find("thead th")->each) {
			push @$fields, decode('UTF-8', $_->text);
		}
	}

	for my $i ($dom->find("tbody tr")->each) {
		my $h= {};
		my @values = $i->find("td")->each;					# html values				
		my $it = each_array(@$fields, @values);
		my @urls;
		while ( my ($x, $y) = $it->() ) {	# start of iteration on each <td></td>	
			$h->{$x}=decode('UTF-8', $y->all_text); 					# could be text, all_tex
			for my $e ($y->find('a[href]')->each) {						# extract all urls;
				push @urls, $e->attr("href");
			  }
			$h->{urls} = \@urls;
			# if (defined $y) {
			# 	push @urls, $y->at("a[href]")->attr("href");
			# }
		}	# end of iteration on each <td></td>
		push @array_of_hashes, $h;
		$h= {};
	}
	return \@array_of_hashes;
}
