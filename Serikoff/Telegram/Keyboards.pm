package Serikoff::Telegram::Keyboards;

use utf8;

use Encode qw(decode);
use JSON::MaybeXS;

use Exporter qw(import);
our @EXPORT_OK = qw(create_one_time_keyboard);

sub create_one_time_keyboard {
	my ($keys, $k_per_row) = @_;

	if (not defined $k_per_row) {
		$k_per_row = scalar @$keys;
	}

	my @keyboard = ();
	my $row = [];
	for (my $i = 0; $i < scalar @$keys; ++$i) {
		if ($i > 0 && $i % $k_per_row == 0) {
			push @keyboard, $row;
			$row = [];
		}

		push @$row, $keys->[$i];
	}
	push @keyboard, $row;

	my %rpl_markup = (
		    keyboard => \@keyboard,
		    one_time_keyboard => JSON::MaybeXS::JSON->true
	);

	my $json = JSON::MaybeXS->new(utf8 => 1);
	return decode('UTF-8', $json->encode(\%rpl_markup));
}
