package Serikoff::Telegram::Restgram;

use strict;
use warnings;

use Mojo::Base -base;
use Mojo::IOLoop;
use Mojo::URL;
use Mojo::UserAgent;
use JSON qw(encode_json);

has service_url => sub { Mojo::URL->new('http://localhost:3000/') };
has ua => sub { Mojo::UserAgent->new() };

sub new {
	my $class = shift;
	my $s = {};
	bless $s, $class;
	return $s;
}

sub _log_error {
	my ($self, $message) = @_;
	print $message . "\n";
}

sub get_full_user_by_phone {
	my ($self, $phone) = @_;

	my $result = undef;

	if (not defined($phone)) {
		$self->_log_error('error: phone is undefined');
		return $result;
	}

	my $import_url = Mojo::URL->new($self->service_url)->path("import_contact")->query(phone => $phone);
	my $import_tx = $self->ua->get($import_url);

	if (not $import_tx->success) {
		my $error_data = defined($import_tx->res->json) ? encode_json($import_tx->res->json) : '';
		$self->_log_error('error: import_contact ' . $import_tx->error->{message} . ' '  . $error_data);
		return $result;
	}

	if ($import_tx->res->json->{status} ne 'ok') {
		my $error_data = defined($import_tx->res->json) ? encode_json($import_tx->res->json) : '';
		$self->_log_error('error: import_contact ' . $error_data);
		return $result;
	}

	my $user_id = $import_tx->res->json->{data}->{user_id};
	$result = { user_id => $user_id };

	my $get_full_user_url = Mojo::URL->new($self->service_url)->path("get_full_user")->query(user_id => $user_id);
	my $get_full_user_tx = $self->ua->get($get_full_user_url);

	if (not $get_full_user_tx->success) {
		my $error_data = defined($get_full_user_tx->res->json) ? encode_json($get_full_user_tx->res->json) : '';
		$self->_log_error('error: get_full_user ' . $get_full_user_tx->error->{message} . ' ' . $error_data);
		return $result;
	}

	if ($get_full_user_tx->res->json->{status} ne 'ok') {
		my $error_data = defined($get_full_user_tx->res->json) ? encode_json($get_full_user_tx->res->json) : '';
		$self->_log_error('error: get_full_user ' . $error_data);
		return $result;
	}

	$result->{first_name} = $get_full_user_tx->res->json->{data}->{first_name};
	$result->{last_name} = $get_full_user_tx->res->json->{data}->{last_name};

	return $result;
}

1;
