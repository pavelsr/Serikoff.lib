package Serikoff::Telegram::Sessions;

use common::sense;


# use Exporter qw(import);
# our @EXPORT_OK = qw(get_last_messages);

# @ISA = qw( MyClass );

# Create new item of class (c)


# sub new {bless({}, $_[0])};

# sub new {
#       my $class = shift;
#       return bless {}, $class;
#   }


# Create a new session object
# Session = saving data 
sub new
{
    my $class = shift;
    my $sess_obj = {};
    $sess_obj->{start_msgs} = shift;
    $sess_obj->{stop_msgs} = shift;
    $sess_obj->{session} = {};
    bless $sess_obj, $class;
    return $sess_obj;  # instance of class (not object)
}


# getter
sub get_all_sessions
{
    my $self = shift;
    return $self->{session};
}


# Collect new message into $session variable which is array of hashes.
# hash like { msg => $update->{message}{text}, add_data => $additional_data }
# add_data can be any data structure (hash, array, etc). e.g. { reply_to_screen => 'some_name' }
# Each element of array = each new message = hash

sub update_sessions {
	my ($self, $update, $add_data) = @_;

		my $chat_id = $update->{message}{chat}{id};
	
		if (grep { $update->{message}{text} eq $_ } @{$self->{start_msgs}}) {
			app->log->info("Start session for chat ".$update->{message}{chat}{first_name}." ".$update->{message}{chat}{last_name});
			$self->{session}->{$chat_id} = [];
		}

		my @active_sess_uids = keys %{$self->{session}};

		if ((grep  { $chat_id eq $_ } @active_sess_uids)) {
			my $hash;
			$hash->{msg} = $update->{message}{text};
			if ($add_data) {
				$hash->{add_data} = $add_data;
			}
			app->log->info("Updating session, previous screen: ".$hash->{add_data});
			push @{$self->{session}->{$chat_id}}, $hash;
		}

		if (grep { $update->{message}{text} eq $_ } @{$self->{stop_msgs}}) {
			app->log->info("Stop session for chat ".$update->{message}{chat}{first_name}." ".$update->{message}{chat}{last_name});
			app->log->info("Session dump:".Dumper $self->{session}->{$chat_id});

			## For debug purpose
			# Call data serialize
			my $text = "You send: ".join('->', @{$self->{session}->{$chat_id}}); ;
			#$api->sendMessage({ chat_id => $chat_id, text => $text });

			delete $self->{session}->{$chat_id};
		}

	return $self->{session};
}


# Return previous session hash by $update object (hash)
# Fields: msg, reply_to_screen
sub last_session {
	my ($self, $update) = shift;
	my $chat_id = $update->{message}{chat}{id};
	if ($self->{session}->{$chat_id}) {   		# for non-first (root) screens.
		my @sess_for_chat = @{$self->{session}->{$chat_id}};
		#my $size = scalar @sess_for_chat;
		#return $sess_for_chat[$size-1];    
		return $sess_for_chat[$#sess_for_chat];   # last elenemt
	} else {
		return undef;
	}
}

sub prev_session {
	my ($self, $update) = shift;
	my $chat_id = $update->{message}{chat}{id};
	if ($self->{session}->{$chat_id}) {   		# for non-first (root) screens.
		my @sess_for_chat = @{$self->{session}->{$chat_id}};
		#my $size = scalar @sess_for_chat;
		#return $sess_for_chat[$size-1];
		if ($sess_for_chat[$#sess_for_chat - 1]) {
			return $sess_for_chat[$#sess_for_chat - 1];
		}    
		   # prev elenemt
	}
	return undef;
}

1;