use strict;
use warnings;

use Test::More tests => 9;

use Mojo::Base -strict;
use Mojolicious;
use Mojo::IOLoop;
use Try::Tiny qw(try catch);

use lib '..';
BEGIN { use_ok('Restgram') || BAIL_OUT('can not use Restgram'); }

my $UNKNOWN_ERROR_RESPONSE = {status => 'error', type => 'unknown'};

my $import_contact_map = {
	'0' => {http => 400, payload => {status => 'error', type => 'unknown'}},
	'1' => {http => 400, payload => {status => 'error', type => 'bad_phone'}},
	'10' => {http => 200, payload => {status => 'ok', data => {user_id => 100}}},
	'11' => {http => 200, payload => {status => 'ok', data => {user_id => 110}}},
	'12' => {http => 200, payload => {status => 'ok', data => {user_id => 120}}}
};

my $get_full_user_map = {
	'100' => {http => 400, payload => {status => 'error', type => 'unknown'}},
	'110' => {http => 400, payload => {status => 'error', type => 'no_such_user'}},
	'120' => {http => 200, payload => {status => 'ok', data => {
		user_id => 120,
		first_name => 'Richard',
		last_name => 'Feynmann'
	}}}
};

# setup mock
my $mock = Mojolicious->new;
$mock->log->level('fatal'); # only log fatal errors to keep the server quiet
$mock->routes->get('/import_contact' => sub {
	my $c = shift;
	my $phone = $c->param('phone');
	if (not $import_contact_map->{$phone}) {
		return $c->render(status => 400, json => $UNKNOWN_ERROR_RESPONSE);
	} else {
		my $response = $import_contact_map->{$phone};
		return $c->render(status => $response->{http}, json => $response->{payload});
	}
});
$mock->routes->get('/get_full_user' => sub {
	my $c = shift;
	my $user_id = $c->param('user_id');
	if (not $get_full_user_map->{$user_id}) {
		return $c->render(status => 400, json => $UNKNOWN_ERROR_RESPONSE);
	} else {
		my $response = $get_full_user_map->{$user_id};
		return $c->render(status => $response->{http}, json => $response->{payload});
	}
});

my $restgram = try { Serikoff::Telegram::Restgram->new(); } catch { BAIL_OUT('unable to create new instance: $_'); };
isa_ok($restgram, 'Serikoff::Telegram::Restgram') || BAIL_OUT('can not instantiate');

$restgram->ua->server->app($mock); # point our UserAgent to our new mock server

# set the URL
$restgram->service_url(Mojo::URL->new('/'));

# actual testing
can_ok($restgram, qw(get_full_user_by_phone));

{
	my $res;

	$res = try { return $restgram->get_full_user_by_phone() } catch { $_; };
	is_deeply($res, undef, 'get_full_user_by_phone: empty call');

	$res = try { return $restgram->get_full_user_by_phone(0) } catch { $_; };
	is_deeply($res, undef, 'get_full_user_by_phone: unknown error');

	$res = try { return $restgram->get_full_user_by_phone(1) } catch { $_; };
	is_deeply($res, undef, 'get_full_user_by_phone: bad_phone');

	$res = try { return $restgram->get_full_user_by_phone(10) } catch { $_; };
	is_deeply($res, {user_id => 100}, 'get_full_user_by_phone: get_full_user unknown error');

	$res = try { return $restgram->get_full_user_by_phone(11) } catch { $_; };
	is_deeply($res, {user_id => 110}, 'get_full_user_by_phone: no such user');

	$res = try { return $restgram->get_full_user_by_phone(12) } catch { $_; };
	is_deeply($res, {
		user_id => 120,
		first_name => 'Richard',
		last_name => 'Feynmann'
	}, 'get_full_user_by_phone: successfull result');
}

