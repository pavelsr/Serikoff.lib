use strict;
use warnings;

use Test::More tests => 15;

use lib '..';
use Screens;

my $data = [
    { 'name' => 'root', 'start_command' => '/root' },
    { 'name' => 'child', 'parent' => 'root', 'start_command' => '/child' },
];

my $screens = Serikoff::Telegram::Screens->new($data);

is_deeply($screens->_get_all_screens, $data, 'sub _get_all_screens');

$screens->_set_curr_screen($data->[1]);
is_deeply($screens->current, $data->[1], 'sub current');

is_deeply($screens->_get_next_screen_by_name($data->[0]->{'name'}), $data->[1], 'sub _get_next_screen_by_name');
is_deeply($screens->_get_next_screen_by_name($data->[1]->{'name'}), undef, 'sub _get_next_screen_by_name(last_screen)');

is_deeply($screens->_get_prev_screen_by_name($data->[0]->{'name'}), undef, 'sub _get_prev_screen_by_name(first_screen)');
is_deeply($screens->_get_prev_screen_by_name($data->[1]->{'name'}), $data->[0], 'sub _get_prev_screen_by_name');

# undef crash
#is_deeply($screens->prev_screen(), $data->[0], 'sub prev_screen');

is_deeply($screens->_get_screen_by_start_cmd($data->[0]->{'start_command'}), $data->[0], 'sub _get_screen_by_start_cmd(0)');
is_deeply($screens->_get_screen_by_start_cmd($data->[1]->{'start_command'}), $data->[1], 'sub _get_screen_by_start_cmd(0)');

$screens->_set_curr_screen($data->[0]);
is_deeply($screens->is_first_screen, 1, 'sub is_first_screen(0)');
is_deeply($screens->is_last_screen, 0, 'sub is_last_screen(0)');

$screens->_set_curr_screen($data->[1]);
is_deeply($screens->is_first_screen, 0, 'sub is_first_screen(1)');
is_deeply($screens->is_last_screen, 1, 'sub is_last_screen(1)');

#is_deeply(, , 'sub get_answ_by_key');

#is_deeply(, , 'sub find_screen');
