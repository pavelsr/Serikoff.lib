package Serikoff::Telegram::Screens;

use common::sense;
use Data::Dumper;

# Create a Screens
# Parameter = hash
# State automat
sub new {
    my $class = shift;
    my $s = {};
    $s->{scrns} = shift; # hash with all screens
    $s->{current_screen} = {};
    $s->{previous_screen} = {};
    bless $s, $class;
    return $s; 
}

# Getter all
sub _get_all_screens {
    my $self = shift;
    return $self->{scrns};
}

# Getter
sub current {
    my $self = shift;
    return $self->{current_screen};
}

# Setter
sub _set_curr_screen {
    my ($self, $scrn) = @_;
    $self->{current_screen} = $scrn;
    return $scrn;
}

# Getter: Get next screen name, analysing 'parent' key and set it to $self->{current_screen} 
# Return hash or undef
sub _get_next_screen_by_name {
	my ($self, $screen_name) = @_;
	for (@{$self->_get_all_screens}) {
		if ($_->{parent} eq $screen_name) {
			return $_;
		}
	}
	return undef;
}


sub _get_prev_screen_by_name {
	my ($self, $screen_name) = @_;
	for (@{$self->_get_all_screens}) {
		if ($_->{parent} eq $screen_name) {
			return $_;
		}
	}
	return undef;
}

sub prev_screen {
	my ($self, $screen_name) = @_;
	return $self->_get_prev_screen_by_name($_->current->{name})
}


# Getter: Find screen which associated with particular 'start_command' key value
# Return hash or undef
sub _get_screen_by_start_cmd {
	my ($self, $msg) = @_;
	for my $s (@{$self->_get_all_screens}) {
		# warn Dumper $s;
		if ($s->{start_command} eq $msg) {   # don't use eq here
			#warn $s->{start_command}." + ".$msg;
			return $s;
		}
	}
	return undef;
}


sub is_last_screen {
    my $self = shift;
    if ($self->_get_next_screen_by_name($self->current->{name})) {
    	return 1;
    }
    return 0;
}


sub is_first_screen {
    my $self = shift;
    if ($self->current->{parent}) {
    	return 0;
    }
    return 1;
}



sub get_answ_by_key {
	my ($self, $msg) = @_;
	my $s = $self->_set_curr_screen->{keyboard};
	for (@$s) {
		if ($msg eq $_->{key}) {
			return $_->{answ};
		}
	}
	return undef;
}

# Getter: Highest-level function
# Analyse text and current screen
# Use get_next_screen_by_name and get_screen_by_start_cmd
# Return hash or undef
# You can use:
# $screens->next_screen('/start') then $screens->current or my $s = screens->next_screen('/start');
# Resolve issue with first and second call

sub find_screen {
	my ($self, $text) = @_;

	my $by_cmd = $self->_get_screen_by_start_cmd($text);

	if ($by_cmd) {
		#app->log->info("Found screen by start command :".$by_cmd->{name});
		$self->_set_curr_screen($by_cmd);
		#warn "find_screen(), by cmd : ".Dumper $self->current;
		return $self->current;
		#return $by_cmd;
	}

	my $by_name = $self->_get_next_screen_by_name($self->current->{name});
	
	if ($by_name) {
		#warn "find_screen(), by name : ".Dumper $by_name;
		$self->_set_curr_screen($by_name); 
		return $self->current;
	}

	warn "Going to return undef";	
	
	return undef; # undef or hash
}



1;