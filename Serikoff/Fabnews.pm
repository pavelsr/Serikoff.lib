package Serikoff::Fabnews;

use warnings;
use Mojo::UserAgent;
use Exporter qw(import);
use Encode;
use Data::Dumper;
use utf8;

use Serikoff::DOM qw(table_to_hash table2array_of_hashes);
use Serikoff::Data qw(merge_hashes);

use feature 'say';

our @EXPORT_OK = qw(parse_lab get_paginated_urls parse_lab_table get_lab_by_page_and_tr);


# This method is doing parsing page like http://fabnews.ru/fablabs/list/fablabs/page2/
# and return hash 
# parse_lab() use Mojo::UserAgent and Mojo::Dom to extract info about lab
	# be carefull with encodings!
	# definition: parse_lab($url);
	# e.g. parse_lab("http://fabnews.ru/fablabs/item/ufo/"); 
	# Result will  be like this:
	# {
	 #          "business_fields" => "3d печать, CAM",
	 #          "foundation_date" => "03 Декабрь 2013",
	 #          "location" => "Россия, Ростов-на-Дону, ул. Мильчакова 5/2 лаб.5а",
	 #          "phone" => "+79885851900",
	 #          "email" => "team@fablab61.ru",
	 #          "website" => "http://fablab61.ru/"
	 #        };
sub parse_lab {
	my $url = shift;
	my $match = {
		foundation_date => "Дата основания",
		website => "Сайт",
		business_fields => "Виды деятельности",
		location => "Местоположение",
		email => "E-mail",
		phone => "Телефон"
	};
	my $h = Serikoff::DOM::table_to_hash($url, ".company-profile-table");
	return merge_hashes($match, $h);
}


sub get_lab_by_page_and_tr {
	my ($page, $tr) = @_;
	my $pages_total = get_paginate_numbers("http://fabnews.ru/fablabs/");
	my $rows_total = parse_lab_table('http://fabnews.ru/fablabs/list/:all/page'.$page.'/');
	# warn $pages_total . scalar @$rows_total;
	if ($page > $pages_total || $tr > scalar @$rows_total) {
		return "not exists";
	} else {
	my $t = parse_lab_table('http://fabnews.ru/fablabs/list/:all/page'.$page.'/');    #array of hashes
	# warn Dumper $t;
	my $hash1 = $t->[$tr];
	my $hash2 = parse_lab($t->[$tr]->{url});
	my $hash3;
	%$hash3 = (%$hash1, %$hash2);
	my $loc = Serikoff::Yandex::geocoder($hash3->{'location'});
	$loc = join(',', split(' ', $loc));
	$hash3->{'gps'} = $loc;
	# $t->[$tr];
	return $hash3;
	}
}

sub parse_lab_table {
	my $url = shift;
	my @fields = ("name", "fabnews_subscribers", "fabnews_rating");
	my $a = Serikoff::DOM::table2array_of_hashes($url, "table", \@fields);
	# filter results
	for (@$a) {
		if ($_->{name} =~ /Последний пост из блога/) {
			my ($name, $last_post) = split("Последний пост из блога:", $_->{name});
			$_->{name} = $name;
			$_->{last_post} = $last_post;
		}
		$_->{url} = $_->{urls}->[0]."/";
	}
	return $a;
}


sub get_paginate_numbers {
	my $url = shift;
	my $ua = Mojo::UserAgent->new;
	my $pagination = $ua->get($url)->res->dom->at(".pagination");
	my @a = $pagination->find("ul")->each;
	my @ref = $a[1]->find("li")->each;
	my $q = scalar @ref;
	my $last_link = $ref[$q-1]->at("a[href]")->attr("href");    # target link with page
	$last_link =~ /page(\d{1})/;							
	return $1;
}

sub get_paginated_urls {
	my $url = shift;
	my $n = get_paginate_numbers ($url);
	my @urls;
	for (my $i=1; $i <= $n; $i++) {
    	push @urls, 'http://fabnews.ru/fablabs/list/:all/page'.$i.'/';
    }
	return \@urls;
}

# could be without first page
sub get_native_paginated_urls {
	my $url_with_pagination = shift;
	my $ua = Mojo::UserAgent->new;
	my $pagination = $ua->get($url_with_pagination)->res->dom->at(".pagination");
	my @a = $pagination->find("ul")->each;
	my @urls;
	for my $e ($a[1]->find("li")->each) {
		if (length $e) {
			my $j = $e->at("a[href]");
			if (length $j || $j ne "") {
				# say $j->attr("href");
				push @urls, $j->attr("href");
			}
		}
	}
	return \@urls;
}
