package Serikoff::Data;
use strict;
use warnings;
use utf8;
use feature 'say';
no warnings 'utf8';

use Exporter qw(import);
    
our @EXPORT_OK = qw(print_hash merge_hashes);

# prints hash in command line json-way
# input argument - hash array
# use no warnings 'utf8';
sub print_hash {
    my $self = shift;
    print "{ \n";
     while( my ($k, $v) = each %$self ) {
        print $k ." : ". $v . "\n";
    }
    print "} \n";
    return 1;
}



# intellectual merge of two hashes
# return new hash with keys from first hash ($fields) and values from second hash ($values)
# all input hashes must be in Perl internal encoding
sub merge_hashes {
    my ($fields, $values) = @_;
    my $result ={};
    while ( my ($i, $j) = each(%$values) ) {
    	# say "key : ". $i. "  encoding:" . Encode::Detect::Detector::detect($i);
    	# say "value :". $j. "  encoding:"  . Encode::Detect::Detector::detect($i);
        my ($new_key) = grep { $fields->{$_} eq $i } keys $fields;
        $result->{$new_key} = $j;       
    }
    return $result;
}

1;