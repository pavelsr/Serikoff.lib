package Serikoff::Skud;

use File::Tail;
use common::sense;
use DBI;
use feature 'say';
use Data::Dumper;

use Exporter qw(import);
our @EXPORT_OK = qw(monitor_file);


my $dbh = DBI->connect('dbi:SQLite:dbname=skud.db',"","");

sub monitor_file {
# Reader control function
# Monitor log from reader, grant access, store all events to database and output log to standart output 
# e.g. monitor_file('reader1.txt', 1, 1, 4, 0);	# card+pin
# assume that monitored file contains strings like that:
# "Reader internal: bits=26 value=12345678"

	my ($reader_log, $num_of_reader_with_pin, $pin_size, $enter) = @_;
	my $file = File::Tail->new(name => $reader_log, maxinterval => 1, adjustafter => 7);

	# say "On that reader pin is required";		
			my @pin;	   # array of inserted pin symbols
			my $flag = 0;  # 'event' variable that set to 1 after card presented
			my $pin_ok;    # correct pin 
			my $user_id;   # current user if it's non-anonymous
			

	while (defined(my $line=$file->read)) { # Read new line of log file
		my $left = 3-scalar @pin;

		my $hash = detect_code_type($line);   # extract reader number and inserted card/code. start of session
		say "Parsed message : ". Dumper $hash;

		if ($hash->{reader_num} == $num_of_reader_with_pin) {

			# type 'card' handler
			if (exists $hash->{card_id}) {
				say "[ CARD_ID ] Чекин карты # ".$hash->{card_id};

				my $res = is_user_in_db($hash->{card_id});
				$user_id = $res->{id};

				my %hash_for_log = (
					reader_id => $hash->{reader_num},
					code=>$hash->{card_id},
					code_type=>$hash->{code_type},
					user_id=> $res->{id}
				);	
				log_to_db(\%hash_for_log, 'log');

				if (%$res) {
					$pin_ok = $res->{pin};
					warn "True pin is : ".$pin_ok;
					$flag = 1;
					say "Ожидаю ввода pin кода... ";
					say "Установлен флаг : ".$flag;
				} else {
						say "Пользователь не найден";
						# Счётчик на три подряд срабатывания
				}
			}
		} else {
			# say "On that reader pin no required";
				my $res = is_user_in_db($hash->{card_id});
					# warn Dumper $res;
				if (%$res) {
					open_door();
					my %hash_for_log = (
						reader_id => $hash->{reader_num}, 
						user_id=> $res->{id}
					);
					# warn Dumper \%hash_for_log;
					log_to_db(\%hash_for_log, 'entries');
				} else {
					say "User haven't found in db, so can't open door";
				}
		}

		# if (exists $hash->{pin_part}) {
		# 	say "yeah";
		# }

		# if ($flag==1) {
		# 	say "yeah2";
		# }

		# type 'pin' handler
		# somehow this code type does not work
		if ((exists $hash->{pin_part}) && ($flag==1)) {    
				warn "a";
				say "[ PIN ] Введено число: ".parse_symbol($hash->{pin_part})." Осталось ввести чисел: ".$left;
				push @pin, parse_symbol($hash->{pin_part});
		}

		# handler last inserted pin symbol
		if (scalar @pin == $pin_size) {	
				my $pin = join("", @pin);
				say "Введён пин код ".$pin;

				my %hash_for_log = (
					reader_id => $hash->{reader_num},
					code=>$pin,
					code_type=>$hash->{code_type},
					user_id=> $user_id
				);	
				log_to_db(\%hash_for_log, 'log');

				if ($pin_ok == $pin) {
					my %hash_for_log = (
					reader_id => $hash->{reader_num},
					user_id=> $user_id
					);	
					open_door();
					log_to_db(\%hash_for_log, 'entries');
				} else {
					say "Введён неверный PIN";
				}
				$pin_ok = '';
				@pin=(); 
				$flag = 0;
				$user_id = '';
		}

	}	# end of line parsing
}	# end of monitor_file sub

# is_user_in_db(12345678);


sub open_door {
	my $pin = shift;
	say "Открываем дверь...";
	system('gpio mode 7 out');
	sleep(3);
	system('gpio mode 7 in');
	return 0;
}


sub is_user_in_db {
	my $card_id = shift;
	my $sth = $dbh->prepare("SELECT id, card_id, pin, name, surname FROM users WHERE card_id = ?")  or die $dbh->errstr;
	$sth->execute($card_id);
	my $hash_ref = $sth->fetchrow_hashref;
	# my $hash_ref = $dbh->selectrow_hashref("SELECT id, card_id, pin, name, surname FROM users WHERE card_id = $card_id");
	# warn Dumper $hash_ref;
	if (%$hash_ref) {
		say "[ USER FOUND ] Карта с номером ".$card_id." принадлежит пользователю ".$hash_ref->{name}." ".$hash_ref->{surname}. "( id #".$hash_ref->{id}." )";
		} else {
		say "Пользователь с номер карты ".$card_id." не найден";	
		}
	# $hash_ref = $dbh->selectrow_hashref("SELECT id, card_id, pin, name, surname FROM users WHERE card_id = ?", $key_field);
	return $hash_ref;
}

sub parse_symbol {
	my $sym = shift;
	# my %matrix = (
	#     "1" => "30",
	#     "2" => "45",
	#     "3" => "60",
	#     "4" => "75",
	#     "5" => "90",
	#     "6" => "105",
	#     "7" => "120",
	#     "8" => "135",
	#     "9" => "150",
	#     "esc" => "165",
	#     "0" => "15",
	#     "ent" => "180",
 #    );
my %matrix = (
	    "1" => "225",
	    "2" => "210",
	    "3" => "195",
	    "4" => "180",
	    "5" => "165",
	    "6" => "150",
	    "7" => "135",
	    "8" => "120",
	    "9" => "105",
	    "esc" => "90",
	    "0" => "240",
	    "ent" => "75",
    );
    my %rm = reverse %matrix;
	my $key = $rm{$sym};
	return $key;
}

sub detect_code_type {
	my $line = shift;
	my $hash = {};
	# if ($line =~ /^Reader_(\d{1}): bits=3 value=(\d{8})/) {
 #   		$hash->{reader_num} = $1;
 #   		$hash->{code_type} = 'pin';
 #   		$hash->{pin_part} = $2;
	# }

	# if ($line =~ /^Reader_(\d{1}): bits=26 value=(\d{8})/) {
 #   		$hash->{reader_num} = $1;
 #   		$hash->{code_type} = 'card';
 #   		$hash->{pin_part} = $2;
	# }

	if ($line =~ /^Reader_(\d{1}):/) {
	  		$hash->{reader_num} = $1;
	}
	if ($line =~ /bits=26 value=(\d{8})/) {
		$hash->{code_type} = 'card';
		$hash->{card_id} = $1;
	}
	if ($line =~ /bits=8 value=(\d{2,3})/) {
		$hash->{code_type} = 'pin';
		$hash->{pin_part} = $1;
			}
	return $hash;
}


sub log_to_db {
	my ($hash, $table_name) = @_;
	my $h = prepare_sql($hash);
	# warn Dumper $h;
	my $a = "INSERT INTO ".$table_name." (".$h->{'fields'}.") VALUES (".$h->{'values'}.")";
	warn $a;
	$dbh->do($a) or die $dbh->errstr;
	# return 0;
}

sub prepare_sql {
	my $hash = shift;
	my @fields;
	my @values;
	foreach my $key ( keys %$hash ) {
		push @fields, $key;
		push @values, "'".$hash->{$key}."'";
	}
	my $new_hash;
	$new_hash->{'fields'} = join(", ", @fields);
	$new_hash->{'values'} = join(", ", @values);
	return $new_hash;
}