package Serikoff::Email;

use Email::Simple;
use Email::Sender::Simple;
use Email::Sender::Transport::SMTPS;

sub send_confirmation_mail {

  my ($email_to, $subject, $body) = (@_);
  my $email_from = 'robot@fabmarkt.ru';

  my $transport = Email::Sender::Transport::SMTPS->new({
    host => 'smtp.yandex.ru',
    port => 465,
    ssl => 'ssl',
    sasl_username => $email_from,
    sasl_password => 'cPu1CLGL',
    debug => 1,
  });

  my $mail_data = Email::Simple->create(
    header => [
      To      => $email_to,
      From    => $email_from,
      Subject => $subject,
    ],
    body => $body,
  );

  Email::Sender::Simple->send($mail_data, { transport => $transport });
  return 0;
};

1;